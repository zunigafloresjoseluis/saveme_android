package com.example.joseluis.saveme001;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;

public class MenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback {
    TextView mensaje1;
    TextView mensaje2;

    final Context context = this;
     // Variables maps

    private GoogleMap mMap;
    private LocationManager locationManager;
    private LocationListener locationListener;
    double latitud,longitud;
    private  Location anterior;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        SupportMapFragment mapFragment=(SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //****************************************



    }



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.info_get) {
            info_user();
        } else if (id == R.id.edit_user) {
            show_xml_vivienda();

        } else if (id == R.id.get_point_user) {
            show_puntos_user();
        } else if (id == R.id.add_point) {
            Intent agregar_punto=new Intent(getApplicationContext(),datos_puntos.class);
            String usuario=getIntent().getStringExtra("User");
            agregar_punto.putExtra("User",usuario);
            startActivity(agregar_punto);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void show_xml_vivienda(){
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.datos_vivienda, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptsView);
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });


        AlertDialog alertDialog = alertDialogBuilder.create();


        alertDialog.show();

    }
    public void info_user(){
        String usuario=getIntent().getStringExtra("User");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Usuario  "+usuario)

                .setTitle("Datos Personales")
                .setCancelable(false)
                .setNeutralButton("Aceptar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }
    public void info_marker(){
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.info_marker,null );
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptsView);

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });



        AlertDialog alertDialog = alertDialogBuilder.create();


        alertDialog.show();

    }
    public void show_puntos_user(){
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.puntos_user, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setView(promptsView);
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });


        AlertDialog alertDialog = alertDialogBuilder.create();


        alertDialog.show();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        locationManager=(LocationManager) getSystemService(LOCATION_SERVICE);

        locationListener=new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {


                if(anterior!=null){
                    if(location.distanceTo(anterior)>30){
                        latitud = location.getLatitude();
                        longitud = location.getLongitude();
                        setLatLong(location.getLatitude(), location.getLongitude());
                        LatLng actual = new LatLng(latitud, longitud);
                        anterior=location;
                        mMap.clear();
                        mMap.addMarker(new MarkerOptions().position(actual).title("Yo").snippet("posicion actual").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                        //  mMap.moveCamera(CameraUpdateFactory.newLatLng(actual));
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(actual));
                        float zoomlevel = 16;
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(actual, zoomlevel));

                    }


                }
                else{

                    latitud = location.getLatitude();
                    longitud = location.getLongitude();
                    setLatLong(location.getLatitude(), location.getLongitude());
                    LatLng actual = new LatLng(latitud, longitud);

                    anterior=location;
                    mMap.clear();
                    mMap.addMarker(new MarkerOptions().position(actual).title("Yo").snippet("posicion actual").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                    //  mMap.moveCamera(CameraUpdateFactory.newLatLng(actual));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(actual));
                    float zoomlevel = 16;
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(actual, zoomlevel));
                        /* clase sincrona*/
                    ArrayList<Double> passing = new ArrayList<Double>();
                    passing.add(location.getLatitude());
                    passing.add(location.getLongitude());
                    Get_Puntos _get=new Get_Puntos();
                    _get.execute(passing);
                }

                // location.distanceTo()



            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {
                Intent i=new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(i);

            }
        };

        geolocalizar();


    }
    @Override
    public void onRequestPermissionsResult(int requestCode,String[] permissions,int[] grantResult){
        switch (requestCode){
            case 10:
                geolocalizar();
                break;
            default:
                break;
        }

    }
    public void geolocalizar(){
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)!=PackageManager.PERMISSION_GRANTED){
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M){
                //requestPermissions();
                //new String[](Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.INTERNET),10
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.INTERNET},10);
            }
            return;
        }
        locationManager.requestLocationUpdates("gps",0,0,locationListener);
    }

    public void setLatLong(double latitud,double longitud){
        this.latitud=latitud;
        this.longitud=longitud;
    }

    // clase interna
    private class Get_Puntos extends AsyncTask<ArrayList<Double>, Void, String> {
        public String getPostDataString(JSONObject params) throws Exception {

            StringBuilder result = new StringBuilder();
            boolean first = true;

            Iterator<String> itr = params.keys();

            while(itr.hasNext()){

                String key= itr.next();
                Object value = params.get(key);

                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(key, "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(value.toString(), "UTF-8"));

            }
            return result.toString();
        }
        @Override
        protected String doInBackground(ArrayList<Double>... passing) {
            try{

                JSONObject postDataParams = new JSONObject();
                ArrayList<Double> passed = passing[0];
                postDataParams.put("latitud", passed.get(0));
                postDataParams.put("longitud", passed.get(1));
                Log.e("params",postDataParams.toString());
                URL host=new URL("http://10.0.0.11:8080/puntos-clasificacion");

                HttpURLConnection conn =(HttpURLConnection) host.openConnection();

                conn.setRequestMethod("POST");
                conn.setRequestProperty("Accept","application/json");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8") );
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in=new BufferedReader(
                            new InputStreamReader(
                                    conn.getInputStream()));
                    StringBuffer sb = new StringBuffer("");
                    String line="";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    sb.toString();
                    return sb.toString();


                }
                else {

                    return  new String("false : "+responseCode);

                }


            }catch (Exception e){
                return e.toString();
            }


        }

        @Override
        protected void onProgressUpdate(Void... params) {
            Toast.makeText(MenuActivity.this, "Tarea Progreso",
                    Toast.LENGTH_SHORT).show();
        }



        @Override
        protected void onPreExecute() {


        }

        @Override
        protected void onPostExecute(String result) {
            String r=result;
            try{
                JSONObject resultado= new JSONObject(r);
                JSONArray a=resultado.getJSONArray("distancias");
                Log.e("result",a.get(0).toString()+"---->");



                for(int i=0;i<a.length();i++){

                    JSONObject aux=new JSONObject(a.get(i).toString());
                    double lat=aux.getDouble("latitud");
                    double lng=aux.getDouble("longitud");
                    LatLng critico = new LatLng(lat, lng);

                    switch (aux.getString("tipo_delito")){
                        case "Asesinato":
                            mMap.addMarker(new MarkerOptions().position(critico).title(aux.getString("nombre")).snippet(aux.getString("descripcion"))
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));

                            break;
                        case "Robo negocio":
                            mMap.addMarker(new MarkerOptions().position(critico).title(aux.getString("nombre")).snippet(aux.getString("descripcion"))
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                            break;
                        case "Robo,Asalto":
                            mMap.addMarker(new MarkerOptions().position(critico).title(aux.getString("nombre")).snippet(aux.getString("descripcion"))
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
                            break;


                    }

                }


            }catch (Exception e){
                Toast.makeText(MenuActivity.this, "Error "+e.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }


        }

        @Override
        protected void onCancelled() {
            Toast.makeText(MenuActivity.this, "Tarea cancelada!",
                    Toast.LENGTH_SHORT).show();
        }
    }


}
