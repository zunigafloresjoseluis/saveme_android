package com.example.joseluis.saveme001;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button login=(Button)findViewById(R.id.entrar);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(view.getContext(),Login.class);
                startActivityForResult(intent,0);
            }
        });
        Button registro=(Button)findViewById(R.id.registrar);
        registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(view.getContext(),Registro.class);
                startActivityForResult(intent,0);
                //    Intent intent=new Intent(view.getContext(),Registre.class);
                //  startActivityForResult(intent,0);
            }
        });
    }
}
