package com.example.joseluis.saveme001;

import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import javax.net.ssl.HttpsURLConnection;


public class Login extends AppCompatActivity {

    String text_user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        EditText usuario=(EditText)findViewById(R.id.editText);
        EditText contra=(EditText)findViewById(R.id.password);

        Button ingresa=(Button)findViewById(R.id.signbutton);
        ingresa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText usuario=(EditText)findViewById(R.id.editText);
                EditText contra=(EditText)findViewById(R.id.password);
                String text_contra=contra.getText().toString();
                text_user =usuario.getText().toString();

                ArrayList<String> passing = new ArrayList<String>();
                passing.add(text_user);
                passing.add(text_contra);
                MiTareaAsincronaDialog tarea=new MiTareaAsincronaDialog();
                tarea.execute(passing);
            }
        });





    }
    private class MiTareaAsincronaDialog extends AsyncTask<ArrayList<String>, Void, String> {
        public String getPostDataString(JSONObject params) throws Exception {

            StringBuilder result = new StringBuilder();
            boolean first = true;

            Iterator<String> itr = params.keys();

            while(itr.hasNext()){

                String key= itr.next();
                Object value = params.get(key);

                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(key, "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(value.toString(), "UTF-8"));

            }
            return result.toString();
        }
        @Override
        protected String doInBackground(ArrayList<String>... passing) {
            try{

                JSONObject postDataParams = new JSONObject();
                ArrayList<String> passed = passing[0];
                postDataParams.put("username", passed.get(0));
                postDataParams.put("password", passed.get(1));
                Log.e("params",postDataParams.toString());
                URL host=new URL("http://10.0.0.11:8080/login");

                HttpURLConnection conn =(HttpURLConnection) host.openConnection();

                conn.setRequestMethod("POST");
                conn.setRequestProperty("Accept","application/json");
                conn.setDoInput(true);
                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(getPostDataString(postDataParams));

                writer.flush();
                writer.close();
                os.close();

                int responseCode=conn.getResponseCode();

                if (responseCode == HttpsURLConnection.HTTP_OK) {

                    BufferedReader in=new BufferedReader(
                            new InputStreamReader(
                                    conn.getInputStream()));
                    StringBuffer sb = new StringBuffer("");
                    String line="";

                    while((line = in.readLine()) != null) {

                        sb.append(line);
                        break;
                    }

                    in.close();
                    sb.toString();
                    return sb.toString();


                }
                else {

                    return  new String("false : "+responseCode);

                }


            }catch (Exception e){
                return e.toString();
            }


        }

        @Override
        protected void onProgressUpdate(Void... params) {

            Toast.makeText(Login.this, "Tarea Progreso",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onPreExecute() {


        }

        @Override
        protected void onPostExecute(String result) {
            String r=result;
            try{
                JSONObject resultado= new JSONObject(r);

                Toast.makeText(Login.this, "Tarea finalizada resultado : "+resultado.toString(),
                        Toast.LENGTH_SHORT).show();

                if(resultado.getString("estado").equals("Acceptado")){
                    Intent mapa=new Intent(getApplicationContext(),info_market_default.class);
                    mapa.putExtra("User",text_user);
                    startActivity(mapa);

                }
                else{
                    Toast.makeText(Login.this, "Datos incorrectos",
                            Toast.LENGTH_SHORT).show();

                }


            }catch (Exception e){
                Toast.makeText(Login.this, "Hubo un erro",
                        Toast.LENGTH_SHORT).show();
            }


        }

        @Override
        protected void onCancelled() {
            Toast.makeText(Login.this, "Tarea cancelada!",
                    Toast.LENGTH_SHORT).show();
        }
    }









}