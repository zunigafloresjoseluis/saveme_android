package com.example.joseluis.saveme001;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import java.util.ArrayList;

public class datos_puntos extends AppCompatActivity {
    private ArrayList<String> telefonos;
    private ArrayAdapter<String> adaptador1;
    private ListView lv1;
    private EditText et1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datos_puntos);

        TextView fecha=(TextView)findViewById(R.id.fecha);

        fecha.setOnFocusChangeListener(new View.OnFocusChangeListener() {

                                           @Override
                                           public void onFocusChange(View v, boolean hasFocus) {
                                               if(hasFocus){
                                                   switch (v.getId()){
                                                       case R.id.fecha:
                                                           DatePickerFragment newFragment = new DatePickerFragment();
                                                           newFragment.show(getSupportFragmentManager(), "datePicker");
                                                           break;

                                                   }

                                               }

                                           }
                                       }
        );
        TextView relog =(TextView)findViewById(R.id.times);
        relog.setOnFocusChangeListener(

                new View.OnFocusChangeListener() {

                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if(hasFocus){
                            switch (v.getId()){
                                case R.id.times:
                                    TimePickerFragment newFragment = new TimePickerFragment();
                                    newFragment.show(getSupportFragmentManager(), "timePicker");
                                    break;
                            }

                        }

                    }
                }
        );
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        String[] letra = {"Bajo","Medio","Regular","Alto"};
        spinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, letra));
        Spinner tipo_riesgo = (Spinner) findViewById(R.id.spinner2);
        String[]  riesgos = {"Asalto","Robo casa habitacion","Asesinato"};
        tipo_riesgo.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,riesgos));
    }

}
