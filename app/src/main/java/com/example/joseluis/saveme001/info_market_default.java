package com.example.joseluis.saveme001;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class info_market_default extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_market_default);
        Button siguiente=(Button)findViewById(R.id.siguiente);
        siguiente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent mapa=new Intent(getApplicationContext(),MenuActivity.class);
                String usuario=getIntent().getStringExtra("User");
                mapa.putExtra("User",usuario);
                startActivity(mapa);
            }
        });
    }
}
